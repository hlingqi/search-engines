package com.lingqi.searcher.indexer.model;

import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;

import java.io.File;


@SpringBootTest
public class DocumentTest {

    @Test
    public void testDocumentConstructor() {
        File rootFile = new File("E:\\java程序\\docs\\api");
        File file = new File("E:\\java程序\\docs\\api\\javax\\sql\\DataSource.html");
        String urlPrefix = "https://docs.oracle.com/javase/8/docs/api/";

        Document document = new Document(file, urlPrefix, rootFile);

        Assert.isTrue(document.getTitle().equals("DataSource"));
        Assert.isTrue(document.getUrl().equals("https://docs.oracle.com/javase/8/docs/api/javax/sql/DataSource.html"));
    }

    @Test
    public void testDocumentContent() {
        File file = new File("E:\\java程序\\indexer\\test.html");
        String s = "Hello World Holo Wald \"Holo Wald\" is our first application.";
        Document document = new Document(file, "/", new File("E:\\java程序\\indexer\\"));
        Assert.isTrue(s.equals(document.getContent()));
    }
}