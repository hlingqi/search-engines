package com.lingqi.searcher.indexer.util;

import lombok.SneakyThrows;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class FileScannerTest {

    @Autowired
    private FileScanner fileScanner;

    @SneakyThrows
    @Test
    public void scanFile() {

        List<File> list = fileScanner.scanFile("E:\\java程序\\index", file -> {
            return file.isFile() && file.getName().endsWith(".json");
        });

        List<String> filenameList = list.stream()
                .map(file -> {
                    try {
                        return file.getCanonicalPath();
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                })
                .sorted()
                .collect(Collectors.toList());

        Assert.isTrue(filenameList.equals(Arrays.asList(
                "E:\\java程序\\index\\forward.json",
                "E:\\java程序\\index\\inverted.json"
        )));
    }
}