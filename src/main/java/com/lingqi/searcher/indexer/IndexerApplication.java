package com.lingqi.searcher.indexer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IndexerApplication {

    public static void main(String[] args) {
        SpringApplication.run(com.lingqi.searcher.indexer.IndexerApplication.class, args);
    }

}
